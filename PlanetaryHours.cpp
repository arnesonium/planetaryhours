/*
  Planetary Hours calculator for the Arduino

  Created by Erik L. Arneson <earneson@arnesonium.com>
  Copyright 2017

  Released under the MIT license.
*/

#include "Arduino.h"
#include "PlanetaryHours.h"

PlanetaryHour::PlanetaryHour (const int32_t zone, const double latitude, const double longitude)
{
  set_zone(zone * ONE_HOUR);
  p_zone = zone;
  set_position(latitude * ONE_DEGREE, longitude * ONE_DEGREE);
}

// For planetary hours, days start at sunrise.
int8_t
PlanetaryHour::dayPlanet (const time_t *inputTime)
{
  struct tm *lt = localtime(inputTime);
  int8_t wday = lt->tm_wday;

  const time_t zoneTime = *inputTime + (p_zone * ONE_HOUR);
  const time_t sunRise = sun_rise(&zoneTime);

  if (*inputTime < sunRise) {
    if (0 == wday)
      wday = 6;
    else
      wday--;
  }
  
  int8_t planet = dayPlanets[wday];
  
  free(lt);

  return planet;
}

int8_t
PlanetaryHour::hourPlanet (const time_t *inputTime)
{
  time_t zoneTime = *inputTime + (p_zone * ONE_HOUR);
  time_t sunRise = sun_rise(&zoneTime);
  time_t sunSet = sun_set(&zoneTime);
  time_t startTime;
  uint32_t s, sd, phd;
  uint8_t ch = 12; // 66% of cases involve night hours.

  // Calculate s (seconds of daylight/nighttime) and startTime (beginning of light/dark period)
  if (*inputTime < sunRise) { // sunrise - yesterday sunset
    zoneTime = zoneTime - ONE_DAY;
    startTime = sun_set(&zoneTime);
    s = sunRise - startTime;
  }
  else if (*inputTime >= sunRise && *inputTime < sunSet) { // current daylight seconds
    startTime = sunRise;
    s = daylight_seconds(&zoneTime);
    ch = 0; // Beginning at sunRise
  }
  else { // *inputTime >= sunSet // tomorrow sunrise - sunset
    zoneTime = zoneTime + ONE_DAY;
    sunRise = sun_rise(&zoneTime);
    startTime = sunSet;
    s = sunRise - sunSet;
  }

  // Calculate sd (delta in seconds from start time)
  sd = *inputTime - startTime;

  // Calculate phd (planetary hour duration)
  phd = s / 12;

  // Calculate ch (current hour base -- number of planetary hours that have elapsed -- starts at 0 or 12)
  ch += sd / phd;

  return (ch + this->dayPlanet(inputTime)) % 7;
}

const char *
PlanetaryHour::planetName(int8_t idx)
{
  return planetNames[idx];
}

// End of file!
