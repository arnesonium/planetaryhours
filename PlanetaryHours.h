#ifndef PlanetaryHours_h
# define PlanetaryHours_h 1

#include <time.h>

/*
 * Planetary hours order: Saturn, Jupiter, Mars, Sun, Venus, Mercury, Moon
 */

#define SATURN 0
#define JUPITER 1
#define MARS 2
#define SUN 3
#define VENUS 4
#define MERCURY 5
#define MOON 6

// These go from 0 for Sunday to 6 for Saturday and define the ruling planet of the day.
const int8_t dayPlanets[] = {
  SUN, MOON, MARS, MERCURY, JUPITER, VENUS, SATURN
};


class PlanetaryHour
{
 public:
  PlanetaryHour(const int32_t, const double, const double);
  int8_t dayPlanet(const time_t *);
  int8_t hourPlanet(const time_t *);
  const char *planetName(int8_t);
 private:
  int32_t p_zone;
  const char *planetNames[7] = {
    "Saturn",
    "Jupiter",
    "Mars",
    "Sun",
    "Venus",
    "Mercury",
    "Moon"
  };
};

#endif
