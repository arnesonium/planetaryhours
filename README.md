# Planetary Hours library for Arduino

This library uses the `sun_rise()` and `sun_set()` functions from the [avr-libc time.h](http://www.nongnu.org/avr-libc/user-manual/group__avr__time.html) library to calculate planetary hours.

For more information on planetary hours, check out these resources:

- http://www.renaissanceastrology.com/planetaryhoursarticle.html
- http://www.esotericarchives.com/agrippa/agripp2c.htm#chap34

The planets themselves are represented numerically from 0 to 6 based on the Chaldean order, like this:

```c++
#define SATURN 0
#define JUPITER 1
#define MARS 2
#define SUN 3
#define VENUS 4
#define MERCURY 5
#define MOON 6
```
