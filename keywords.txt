#######################################
# Syntax Coloring Map For Adafruit_NeoPixel
####################################### 
# Class
#######################################

PlanetaryHour	KEYWORD1

#######################################
# Methods and Functions 
#######################################	

dayPlanet	KEYWORD2
hourPlanet	KEYWORD2

#######################################
# Constants
#######################################

SATURN			LITERAL1
JUPITER			LITERAL1
MARS			LITERAL1
SUN			LITERAL1
VENUS			LITERAL1
MERCURY			LITERAL1
MOON			LITERAL1
